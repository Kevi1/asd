import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './layout/nav-bar/nav-bar.component';
import { FullLayoutComponent } from './layout/full-layout/full-layout.component';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreModule } from './state-management';
import { HomeComponent } from './layout/home/home.component';
import { UserModule } from './user/user.module';
import { SharedModule } from './shared/shared.module';
import { CustomerModule } from './customer/customer.module';
import { ProviderModule } from './provider/provider.module';
import { AuthInterceptor } from './auth.interceptor';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { JwtHelperService, JWT_OPTIONS } from '@auth0/angular-jwt';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MessageService } from "primeng/api";
@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    FullLayoutComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule,
    StoreDevtoolsModule.instrument(),
    UserModule,
    SharedModule,
    CustomerModule,
    ProviderModule,
    HttpClientModule,
    BrowserAnimationsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
    JwtHelperService,
    MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
