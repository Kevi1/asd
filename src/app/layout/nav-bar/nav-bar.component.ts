import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { UserFacade } from 'src/app/state-management/user';

@Component({
  selector: 'nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
  role!: string | null
  isCustomer: boolean = false;
  isProvider: boolean = false;

  constructor(private userFacade: UserFacade, private authService: AuthService) { }

  ngOnInit(): void {
    this.userFacade.role$.subscribe(role => {
      this.role = this.authService.getUserRole() ?? role
      if (!this.role) {
        return
      }

      if (this.role === "customer") {
        this.isCustomer = true
      }

      if (this.role === "provider") {
        this.isProvider = true
      }
    })

  }

  logout() {
    this.authService.logout()
  }

}
