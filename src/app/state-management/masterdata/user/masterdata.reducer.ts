import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Action, createReducer, on } from '@ngrx/store';

import { MasterdataActions } from './masterdata.actions';

export const USER_FEATURE_KEY = 'user';

//User instead of any
export interface MasterdataState extends EntityState<any> {
  healthGoals: string[];
}

export interface UserPartialState {
  readonly [USER_FEATURE_KEY]: MasterdataState;
}


export const adapter: EntityAdapter<any> = createEntityAdapter<any>();

export const initialState: MasterdataState = adapter.getInitialState({
  healthGoals: [],
});

const reducer = createReducer(
  initialState,
  // on(UserActions.currentUserLoaded, (state: UserState, { payload }) => ({
  //   ...state,
  //   currentUser: payload,
  // })),
  // on(UserActions.currentUserRolesLoaded, (state: UserState, { payload }) => ({
  //   ...state,
  //   currentUserRoles: payload,
  // }))
);

export function userReducer(state: MasterdataState, action: Action) {
  return reducer(state, action);
}
