import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule as NgrxStoreModule } from '@ngrx/store';

import { UserEffects } from './masterdata.effects';
import { USER_FEATURE_KEY, userReducer } from './masterdata.reducer';

@NgModule({
  imports: [
    CommonModule,
    NgrxStoreModule.forFeature(USER_FEATURE_KEY, userReducer),
    EffectsModule.forFeature([UserEffects]),
  ],
})
export class MasterdataModule { }
