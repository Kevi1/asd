import { createAction, props } from '@ngrx/store';
import { Payload } from '../../store.model';


export const loadHealthGoals = createAction('[Masterdata] Load Health Goals');

export const healthGoalsLoaded = createAction(
  '[Masterdata] Health Goals loaded',
  props<Payload<string[]>>()
);

export const healthGoalsError = createAction(
  '[Masterdata] Load Health Goals Error',
  // props<ErrorPayload>()
);

export const loadCurrentUserRoles = createAction(
  '[Masterdata] Load Current User Roles'
);

export const currentUserRolesLoaded = createAction(
  '[User] Current User Roles Loaded',
  // props<Payload<string[]>>()
);

export const loadCurrentUserRolesError = createAction(
  '[User] Load Current User Roles Error',
  // props<ErrorPayload>()
);

export const MasterdataActions = {
  loadHealthGoals,
  healthGoalsLoaded,
  healthGoalsError,
  loadCurrentUserRoles,
  currentUserRolesLoaded,
  loadCurrentUserRolesError,
};
