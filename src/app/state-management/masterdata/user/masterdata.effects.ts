import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { MasterdataActions } from './masterdata.actions';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { exhaustMap } from 'rxjs-compat/operator/exhaustMap';

// import { UserActions } from './user.actions';

@Injectable()
export class UserEffects {
  // loadHealthGoals$ = createEffect(() =>
  //   this.actions$.pipe(
  //     ofType(MasterdataActions.loadHealthGoals),
  //     exhaustMap(action =>
  //       this.authService.healthGoals().pipe(
  //         map(response => {
  //           console.log("response:::", response)
  //           return MasterdataActions.healthGoalsLoaded({response})
  //         }),
  //         catchError((error: any) => of(MasterdataActions.healthGoalsError(error))))
  //     )
  //   )
  // );
  // loadCurrentUserRoles$ = createEffect(() =>
  // this.actions$.pipe(
  //   ofType(UserActions.loadCurrentUserRoles),
  //   switchMap(() =>
  //     this.userService.getCurrentUserRoles().pipe(
  //       map((roleCollection: RoleCollection) => roleCollection.roles),
  //       map((roles) => {
  //         if (roles.length === 0) this.router.navigate(['/pages', '403']);
  //         return UserActions.currentUserRolesLoaded({ payload: roles });
  //       }),
  //       catchError((error) =>
  //         of(UserActions.loadCurrentUserRolesError({ payload: error }))
  //       )
  //     )
  //   )
  // )
  // );

  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private router: Router
  ) { }
}
