import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import { MasterdataActions } from './masterdata.actions';
import { UserPartialState } from './masterdata.reducer';
import { userQuery } from './masterdata.selectors';

@Injectable({
  providedIn: 'root',
})
export class MasterdataFacade {
  healthGoals$ = this.store.select(userQuery.getHealthGoals);

  constructor(private store: Store<UserPartialState>) { }

  initialize() {
    this.store.dispatch(MasterdataActions.loadHealthGoals());
  }
}
