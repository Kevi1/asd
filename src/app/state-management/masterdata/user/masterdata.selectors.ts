import { createFeatureSelector, createSelector } from '@ngrx/store';

import { USER_FEATURE_KEY, MasterdataState } from './masterdata.reducer';

const getUserState = createFeatureSelector<MasterdataState>(USER_FEATURE_KEY);

const getHealthGoals = createSelector(
  getUserState,
  (state: MasterdataState) => state.healthGoals
);

export const userQuery = {
  getHealthGoals
};
