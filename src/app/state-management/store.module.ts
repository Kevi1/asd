import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule as NgrxStoreModule } from '@ngrx/store';

import { ActionResolver } from './action.resolver';
import { routerReducer } from '@ngrx/router-store';
import { UserModule } from './user/user.module';

@NgModule({
  imports: [
    CommonModule,
    NgrxStoreModule.forRoot({
      router: routerReducer
    }, {
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true,
        strictActionSerializability: false,
        strictStateSerializability: true
      }
    }),
    EffectsModule.forRoot([]),
    UserModule,
  ],
  providers: [ActionResolver]
})
export class StoreModule { }