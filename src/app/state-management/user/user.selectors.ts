import { createFeatureSelector, createSelector } from '@ngrx/store';

import { USER_FEATURE_KEY, UserState } from './user.reducer';

const getUserState = createFeatureSelector<UserState>(USER_FEATURE_KEY);

const getCurrentUser = createSelector(
  getUserState,
  (state: UserState) => state.currentUser
);
const getCurrentUserRoles = createSelector(
  getUserState,
  (state: UserState) => state.currentUserRoles
);

const getRole = createSelector(
  getUserState,
  (state: UserState) => state.role
);


export const userQuery = {
  getCurrentUser,
  getCurrentUserRoles,
  getRole
};
