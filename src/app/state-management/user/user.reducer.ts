import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Action, createReducer, on } from '@ngrx/store';

import { UserActions } from './user.actions';

export const USER_FEATURE_KEY = 'user';

//User instead of any
export interface UserState extends EntityState<any> {
  currentUser: any;
  currentUserRoles: string[];
  role: string | null
}

export interface UserPartialState {
  readonly [USER_FEATURE_KEY]: UserState;
}

export const adapter: EntityAdapter<any> = createEntityAdapter<any>();

export const initialState: UserState = adapter.getInitialState({
  currentUser: null,
  currentUserRoles: [],
  role: null
});

const reducer = createReducer(
  initialState,
  on(UserActions.setRole, (state: UserState, action) => ({
    ...state,
    role: action.payload,
  })),
  // on(UserActions.currentUserRolesLoaded, (state: UserState, { payload }) => ({
  //   ...state,
  //   currentUserRoles: payload,
  // }))
);

export function userReducer(state: UserState, action: Action) {
  return reducer(state, action);
}
