import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import { UserActions } from './user.actions';
import { UserPartialState } from './user.reducer';
import { userQuery } from './user.selectors';

@Injectable({
  providedIn: 'root',
})
export class UserFacade {
  currentUserRoles$ = this.store.select(userQuery.getCurrentUserRoles);
  currentUser$ = this.store.select(userQuery.getCurrentUser);
  role$ = this.store.select(userQuery.getRole);

  constructor(private store: Store<UserPartialState>) { }

  initialize() {
    this.store.dispatch(UserActions.loadCurrentUser());
    this.store.dispatch(UserActions.loadCurrentUserRoles());
  }

  setRole(role: string | null) {
    this.store.dispatch(UserActions.setRole({ payload: role }));
  }

}
