import { createAction, props } from '@ngrx/store';

import { Payload } from '../store.model';

export const loadCurrentUser = createAction('[User] Load current User');

export const currentUserLoaded = createAction(
  '[User] Current User loaded',
  // props<Payload<User>>()
);

export const loadCurrentUserError = createAction(
  '[User] Load Current User Error',
  // props<ErrorPayload>()
);

export const loadCurrentUserRoles = createAction(
  '[User] Load Current User Roles'
);

export const currentUserRolesLoaded = createAction(
  '[User] Current User Roles Loaded',
  // props<Payload<string[]>>()
);

export const loadCurrentUserRolesError = createAction(
  '[User] Load Current User Roles Error',
  // props<ErrorPayload>()
);

export const setRole = createAction(
  '[User] Set role',
  props<Payload<any>>()
);

export const UserActions = {
  loadCurrentUser,
  currentUserLoaded,
  loadCurrentUserError,
  loadCurrentUserRoles,
  currentUserRolesLoaded,
  loadCurrentUserRolesError,
  setRole
};
