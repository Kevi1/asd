import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule as NgrxStoreModule } from '@ngrx/store';

import { UserEffects } from './user.effects';
import { USER_FEATURE_KEY, userReducer } from './user.reducer';

@NgModule({
  imports: [
    CommonModule,
    NgrxStoreModule.forFeature(USER_FEATURE_KEY, userReducer),
    EffectsModule.forFeature([UserEffects]),
  ],
})
export class UserModule { }
