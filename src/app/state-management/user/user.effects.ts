import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { UserActions } from './user.actions';

@Injectable()
export class UserEffects {
  // loadCurrentUser$ = createEffect(() =>
  // this.actions$.pipe(
  //   ofType(UserActions.loadCurrentUser),
  //   switchMap(() =>
  //     this.userService.getCurrentUser().pipe(
  //       map((currentUser) =>
  //         UserActions.currentUserLoaded({ payload: currentUser })
  //       ),
  //       catchError((error) =>
  //         of(UserActions.loadCurrentUserError({ payload: error }))
  //       )
  //     )
  //   )
  // )
  // );

  // loadCurrentUserRoles$ = createEffect(() =>
  // this.actions$.pipe(
  //   ofType(UserActions.loadCurrentUserRoles),
  //   switchMap(() =>
  //     this.userService.getCurrentUserRoles().pipe(
  //       map((roleCollection: RoleCollection) => roleCollection.roles),
  //       map((roles) => {
  //         if (roles.length === 0) this.router.navigate(['/pages', '403']);
  //         return UserActions.currentUserRolesLoaded({ payload: roles });
  //       }),
  //       catchError((error) =>
  //         of(UserActions.loadCurrentUserRolesError({ payload: error }))
  //       )
  //     )
  //   )
  // )
  // );

  constructor(
    private actions$: Actions,
    // private userService: UserService,
    private router: Router
  ) { }
}
