import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth/auth.service';
import { UserFacade } from 'src/app/state-management/user';

@Injectable({
  providedIn: 'root'
})
export class UserGuard implements CanActivate {
  role: any;
  constructor(private authService: AuthService, private router: Router, private userFacade: UserFacade) {
    this.userFacade.role$.subscribe(role => {
      this.role = role
      console.log(role);
    })
  }

  canActivate(): boolean {
    const userRole = this.authService.getUserRole();
    this.role = userRole ?? this.role

    if (this.role === "customer") {
      // Allow access for admin
      return true;
    } else {
      // Redirect to login page for other users
      this.router.navigate(['/login']);
      return false;
    }
  }
}
