import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'meal-card',
  templateUrl: './meal-card.component.html',
  styleUrls: ['./meal-card.component.scss']
})
export class MealCardComponent implements OnInit {
  @Input() title!: string
  @Input() subtitle!: string
  @Input() description!: string
  @Input() envImpact!: string

  constructor() { }

  ngOnInit(): void {
  }

}
