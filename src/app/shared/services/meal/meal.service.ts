import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root',
})
export class MealService {
  private apiUrl = `${environment.apiUrl}`;

  constructor(private http: HttpClient) {}
  headerDict = {
    Authorization: ` Bearer ${localStorage.getItem('access_token')}` ?? 'asd',
  };
  requestOptions = {
    headers: new HttpHeaders(this.headerDict),
  };

  addMeal(meal: any, file: any, provider_id: number) {
    this.http
      .post<any>(
        `${this.apiUrl}/meals/meals/`,
        { ...meal?.value, file, provider: provider_id },
        this.requestOptions
      )
      .subscribe({
        next: (res) => {
          // this.router.navigate(['/login']);
        },
        error: (error) => {
          console.log(error);
          // return this.router.navigate(['/404']);
        },
      });
  }
}
