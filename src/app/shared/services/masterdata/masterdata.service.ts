import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class MasterdataService {
  private apiUrl = `${environment.apiUrl}/meals`;

  constructor(private http: HttpClient) { }

  healthGoals() {
    return this.http.get<any>(`${this.apiUrl}/health-goals`)
  }

  dietPreferences() {
    return this.http.get<any>(`${this.apiUrl}/diet-preferences`)
  }

  cuisine() {
    return this.http.get<any>(`${this.apiUrl}/cuisines`)
  }

  cookingStyles() {
    return this.http.get<any>(`${this.apiUrl}/cooking-styles`)
  }

  environmentalImpact() {
    return this.http.get<any>(`${this.apiUrl}/environmental-impacts`)
  }


}
