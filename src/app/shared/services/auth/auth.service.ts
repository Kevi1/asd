import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { catchError, map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import { UserFacade } from 'src/app/state-management/user';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private apiUrl = `${environment.apiUrl}/auth`;
  constructor(
    private http: HttpClient,
    private jwtHelper: JwtHelperService,
    private router: Router,
    private userFacade: UserFacade
  ) {}

  login(email: string, password: string) {
    console.log('login');

    this.http.post<any>(`${this.apiUrl}/login`, { email, password }).subscribe({
      next: (res) => {
        localStorage.setItem('access_token', res.data.access);
        localStorage.setItem('refresh_token', res.data.refresh);
        localStorage.setItem('role', res.data.role);
        this.userFacade.setRole(res.data.role);
        this.router.navigate(['/quiz']);
      },
      error: (error) => {
        return this.router.navigate(['/404']);
      },
    });
  }

  register(
    firstName: string,
    lastName: string,
    email: string,
    password: string,
    password2: string,
    role: string
  ) {
    console.log('login');

    this.http
      .post<any>(`${this.apiUrl}/register`, {
        firstName,
        lastName,
        email,
        password,
        password2,
        role,
      })
      .subscribe({
        next: (res) => {
          this.router.navigate(['/login']);
        },
        error: (error) => {
          return this.router.navigate(['/404']);
        },
      });
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('access_token');
    localStorage.removeItem('refresh_token');
    this.userFacade.setRole(null);
  }

  refreshToken(): Observable<boolean> {
    const refreshToken = localStorage.getItem('refresh_token');
    return this.http
      .post<{ access_token: string }>(`${this.apiUrl}/refresh-token`, {
        refresh_token: refreshToken,
      })
      .pipe(
        map((result) => {
          localStorage.setItem('access_token', result.access_token);
          return true;
        })
      );
  }

  isAuthenticated(): boolean {
    const token = localStorage.getItem('access_token');
    return !this.jwtHelper.isTokenExpired(token);
  }

  getUsername(): string | null {
    const token = localStorage.getItem('access_token');
    if (token) {
      const decodedToken = this.jwtHelper.decodeToken(token);
      return decodedToken ? decodedToken.username : null;
    } else {
      return null;
    }
  }
  getId(): string | null {
    const token = localStorage.getItem('access_token');
    if (token) {
      const decodedToken = this.jwtHelper.decodeToken(token);
      console.log(decodedToken);
      return decodedToken ? decodedToken.user_id : null;
    } else {
      return null;
    }
  }

  getAccessToken(): string | null {
    return localStorage.getItem('access_token');
  }

  getUserRole(): string | null {
    return localStorage.getItem('role')
  }
}
