import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { MealCardComponent } from './components/meal-card/meal-card.component';
import { RadioButtonModule } from 'primeng/radiobutton';
import { CardModule } from 'primeng/card';
import { MultiSelectModule } from 'primeng/multiselect';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StepsModule } from 'primeng/steps';
import { ToastModule } from 'primeng/toast';
@NgModule({
  declarations: [
    MealCardComponent
  ],
  imports: [
    CommonModule,
    InputTextModule,
    ButtonModule,
    RadioButtonModule,
    CardModule,
    MultiSelectModule,
    FormsModule,
    ReactiveFormsModule,
    StepsModule,
    ToastModule,
  ],
  exports: [
    InputTextModule,
    ButtonModule,
    RadioButtonModule,
    CardModule,
    MealCardComponent,
    MultiSelectModule,
    FormsModule,
    ReactiveFormsModule,
    StepsModule,
    ToastModule
  ]
})
export class SharedModule { }
