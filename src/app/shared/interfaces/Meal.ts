export interface Meal {
    image: string;
    description: string;
    ingredients: string[];
    dietPreferences: string[];
    healthGoals: string[];
    cookingStyle: string[];
    cuisine: string[];
    environmentalImpact: string[];
    category: string[];
}