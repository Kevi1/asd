import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { P404Component } from './pages/404.component';
import { FullLayoutComponent } from './layout/full-layout/full-layout.component';
import { HomeComponent } from './layout/home/home.component';
import { LoginComponent } from './user/login/login.component';
import { RegisterComponent } from './user/register/register.component';
import { CustomerHomeComponent } from './customer/customer-home/customer-home.component';
import { UserGuard } from './shared/guards/user.guard';
import { StepperComponent } from './customer/stepper/stepper.component';
import { AddMealComponent } from './provider/addMeal/addMeal.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: '',
    component: FullLayoutComponent,
    data: {
      title: 'Home',
    },
  },
  {
    path: 'home',
    component: HomeComponent,
    data: {
      title: 'Home',
    },
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Home',
    },
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Home',
    },
  },
  {
    path: 'quiz',
    component: StepperComponent,
    data: {
      title: 'Home',
    },
    canActivate: [UserGuard],
  },
  {
    path: 'meals',
    component: CustomerHomeComponent,
    data: {
      title: 'Home',
    },
    canActivate: [UserGuard],
  },
  {
    path: 'add-meal',
    component: AddMealComponent,
    data: {
      title: 'Add meal',
    },
    // canActivate: [UserGuard],
  },
  {
    path: '',
    data: {
      title: 'Example Pages',
    },
    children: [
      {
        path: '404',
        component: P404Component,
        data: {
          title: 'Page 404',
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
