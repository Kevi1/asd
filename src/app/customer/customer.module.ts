import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { CustomerHomeComponent } from './customer-home/customer-home.component';
import { MealsComponent } from './meals/meals.component';
import { StepperComponent } from './stepper/stepper.component';


@NgModule({
  declarations: [
    CustomerHomeComponent,
    MealsComponent,
    StepperComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class CustomerModule { }
