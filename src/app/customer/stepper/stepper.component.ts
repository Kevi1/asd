import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { MasterdataService } from 'src/app/shared/services/masterdata/masterdata.service';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss']
})
export class StepperComponent implements OnInit {
  items!: MenuItem[];

  activeIndex: number = 0;

  steperForm: FormGroup;
  categoriesList!: Object[];
  healthGoalsList!: string[];
  dietPreferencesList!: string[];
  cuisineList!: string[];
  cookingStylesList!: string[];
  environmentalImpactList!: string[];

  constructor(public messageService: MessageService, private formBuilder: FormBuilder, private masterdataService: MasterdataService, private router: Router) {
    this.steperForm = this.formBuilder.group({
      category: [null],
      dietPreferences: [null],
      ingredients: [null],
      healthGoals: [null],
      cookingStyle: [null],
      cuisine: [null],
      preparationTime: [null],
      environmentalImpactFeature: [null],
    });

    this.categoriesList = [
      {
        id: 1,
        name: "walking_delivery",
        description: "Breakfast"
      },
      {
        id: 2,
        name: "walking_delivery",
        description: "Lunch"
      },
      {
        id: 3,
        name: "walking_delivery",
        description: "Dinner"
      },
    ];
  }

  onActiveIndexChange(event: any) {
    this.activeIndex = event;
    console.log(this.activeIndex);
  }



  ngOnInit() {
    this.items = [
      {
        label: 'Ingredients/Allergies',
        command: (event: any) => this.messageService.add({ severity: 'info', summary: 'First Step', detail: event.item.label })
      },
      {
        label: 'Health Goals',
        command: (event: any) => this.messageService.add({ severity: 'info', summary: 'Second Step', detail: event.item.label })
      },
      {
        label: 'Cuisine',
        command: (event: any) => this.messageService.add({ severity: 'info', summary: 'Third Step', detail: event.item.label })
      },
      {
        label: 'Cooking style',
        command: (event: any) => this.messageService.add({ severity: 'info', summary: 'Fourth Step', detail: event.item.label })
      },
      {
        label: 'Environmental impact',
        command: (event: any) => this.messageService.add({ severity: 'info', summary: 'Last Step', detail: event.item.label })
      }
    ];

    //get all masterdata
    this.masterdataService.healthGoals().subscribe({
      next: res => {
        this.healthGoalsList = res.data
      }
    })

    this.masterdataService.dietPreferences().subscribe({
      next: res => {
        this.dietPreferencesList = res.data
      }
    })
    this.masterdataService.cuisine().subscribe({
      next: res => {
        this.cuisineList = res.data
      }
    })
    this.masterdataService.cookingStyles().subscribe({
      next: res => {
        this.cookingStylesList = res.data
      }
    })
    this.masterdataService.environmentalImpact().subscribe({
      next: res => {
        this.environmentalImpactList = res.data
      }
    })
  }

  get category() {
    return this.steperForm.get('category');
  }
  get dietPreferences() {
    return this.steperForm.get('dietPreferences');
  }
  get ingredients() {
    return this.steperForm.get('ingredients');
  }
  get healthGoals() {
    return this.steperForm.get('healthGoals');
  }
  get cookingStyle() {
    return this.steperForm.get('cookingStyle');
  }
  get cuisine() {
    return this.steperForm.get('cuisine');
  }
  get preparationTime() {
    return this.steperForm.get('preparationTime');
  }
  get environmentalImpactFeature() {
    return this.steperForm.get('environmentalImpactFeature');
  }

  next(number: number) {
    this.activeIndex = number
  }

  finish() {
    console.log("submit", this.steperForm);
    this.router.navigate(["/meals"])
  }
}
