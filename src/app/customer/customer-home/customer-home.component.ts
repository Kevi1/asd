import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MasterdataService } from 'src/app/shared/services/masterdata/masterdata.service';

@Component({
  selector: 'app-customer-home',
  templateUrl: './customer-home.component.html',
  styleUrls: ['./customer-home.component.scss']
})
export class CustomerHomeComponent implements OnInit, OnDestroy {
  filterForm: FormGroup;
  categoriesList: Object[];
  subscriptions: Subscription[] = [];
  healthGoalsList!: string[];
  dietPreferencesList!: string[];
  cuisineList!: string[];
  cookingStylesList!: string[];
  environmentalImpactList!: string[];

  constructor(private formBuilder: FormBuilder, private masterdataService: MasterdataService) {
    this.filterForm = this.formBuilder.group({
      category: [null],
      dietPreferences: [null],
      ingredients: [null],
      healthGoals: [null],
      cookingStyle: [null],
      cuisine: [null],
      preparationTime: [null],
      environmentalImpactFeature: [null],
    });

    this.categoriesList = [
      {
        id: 1,
        name: "walking_delivery",
        description: "Breakfast"
      },
      {
        id: 2,
        name: "walking_delivery",
        description: "Lunch"
      },
      {
        id: 3,
        name: "walking_delivery",
        description: "Dinner"
      },
    ];
  }

  ngOnInit(): void {
    this.detectChanges();

    //get all masterdata
    this.masterdataService.healthGoals().subscribe({
      next: res => {
        this.healthGoalsList = res.data
      }
    })

    this.masterdataService.dietPreferences().subscribe({
      next: res => {
        this.dietPreferencesList = res.data
      }
    })
    this.masterdataService.cuisine().subscribe({
      next: res => {
        this.cuisineList = res.data
      }
    })
    this.masterdataService.cookingStyles().subscribe({
      next: res => {
        this.cookingStylesList = res.data
      }
    })
    this.masterdataService.environmentalImpact().subscribe({
      next: res => {
        this.environmentalImpactList = res.data
      }
    })
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  detectChanges() {
    this.subscriptions.push(
      this.filterForm.valueChanges.subscribe((filterForm) => {
        console.log(filterForm);

      })
    );
  }

  get category() {
    return this.filterForm.get('category');
  }
  get dietPreferences() {
    return this.filterForm.get('dietPreferences');
  }
  get ingredients() {
    return this.filterForm.get('ingredients');
  }
  get healthGoals() {
    return this.filterForm.get('healthGoals');
  }
  get cookingStyle() {
    return this.filterForm.get('cookingStyle');
  }
  get cuisine() {
    return this.filterForm.get('cuisine');
  }
  get preparationTime() {
    return this.filterForm.get('preparationTime');
  }
  get environmentalImpactFeature() {
    return this.filterForm.get('environmentalImpactFeature');
  }
}
