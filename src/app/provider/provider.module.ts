import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddMealComponent } from './addMeal/addMeal.component';

import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
@NgModule({
  declarations: [AddMealComponent],
  imports: [CommonModule, SharedModule, RouterModule],
})
export class ProviderModule {}
