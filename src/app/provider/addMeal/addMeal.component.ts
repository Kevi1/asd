import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MealService } from 'src/app/shared/services/meal/meal.service';
import { MasterdataService } from 'src/app/shared/services/masterdata/masterdata.service';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
@Component({
  selector: 'app-add-meal',
  templateUrl: './addMeal.component.html',
  styleUrls: ['./addMeal.component.scss'],
})
export class AddMealComponent implements OnInit {
  mealForm: FormGroup;

  // categoriesList: Object[];
  healthGoalsList!: string[];
  dietPreferencesList!: string[];
  cuisineList!: string[];
  cookingStylesList!: string[];
  environmentalImpactList!: string[];
  ingredientsList: string[] = [];
  file: any;
  constructor(
    private formBuilder: FormBuilder,
    private masterdataService: MasterdataService,
    private mealService: MealService,
    private authService: AuthService
  ) {
    this.mealForm = this.formBuilder.group({
      diets: [null],
      health_goals: [null],
      selectedCuisine: [null],
      cooking_styles: [null],
      selectedIngredient: [null],
      name: [null],
      description: [null],
      category: [null],
      preparationTime: [null],
      price: [null],
      preperation_time: [null],
    });
  }

  onChange(event: any) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      console.log(file);
      this.file = file;
    }
  }
  get diets() {
    return this.mealForm.get('selectedDietPreference');
  }

  get health_goals() {
    return this.mealForm.get('health_goals');
  }

  get selectedCuisine() {
    return this.mealForm.get('selectedCuisine');
  }

  get selectedCookingSytle() {
    return this.mealForm.get('selectedCookingSytle');
  }

  get selectedIngredient() {
    return this.mealForm.get('selectedIngredient');
  }

  get name() {
    return this.mealForm.get('name');
  }

  get description() {
    return this.mealForm.get('description');
  }

  get category() {
    return this.mealForm.get('category');
  }

  get preparationTime() {
    return this.mealForm.get('preparationTime');
  }

  get price() {
    return this.mealForm.get('price');
  }

  // get image() {
  //   return this.mealForm.get('image');
  // }

  ngOnInit(): void {
    // this.detectChanges();

    //get all masterdata
    this.masterdataService.healthGoals().subscribe({
      next: (res) => {
        this.healthGoalsList = res.data;
        console.log(res.data);
      },
    });

    this.masterdataService.dietPreferences().subscribe({
      next: (res) => {
        this.dietPreferencesList = res.data;
      },
    });
    this.masterdataService.cuisine().subscribe({
      next: (res) => {
        this.cuisineList = res.data;
      },
    });
    this.masterdataService.cookingStyles().subscribe({
      next: (res) => {
        this.cookingStylesList = res.data;
      },
    });
    this.masterdataService.environmentalImpact().subscribe({
      next: (res) => {
        this.environmentalImpactList = res.data;
      },
    });
  }

  submit() {
    const idString = this.authService.getId();
    const id = idString ? parseInt(idString) : 0;
    this.mealService.addMeal(this.mealForm, this.file, id);
    // this.authService.login(this.email?.value, this.password?.value);
  }
}
